<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
</head>
<body>
	<header>
		<nav>
			<a href="/">Home</a>
			<a href="/blog">Blog</a>
		</nav>
	</header>
	@yield('content')
	<footer>
		<p>
			&copy; laravel & sekolahkoding 2016
		</p>
	</footer>
</body>
</html>